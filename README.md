# Example spring-boot in-men CRUD app

http://projects.spring.io/spring-boot

## Steps

*clone the project*

$ git clone project

*build and test your app*

$ cd project

$ mvn spring-boot:run

*package the app*

$ mvn package

*build a container*

$ docker build -t inmemcrud:dev .

*run container*

$ docker run -it -d -p 8080:8080 inmemcrud:dev

## Using

See which urls are available:

$ curl localhost:8080

View existing objects:

$ curl localhost:8080/books

Create new ones:

$ curl -i -X POST -H "Content-Type:application/json" -d '{ "title" : "Generation X", "author" : "Douglas Coupland" }' localhost:8080/books

See results:

$ curl localhost:8080/books/1

## Post steps

The H2 in-mem module helps us develop the app quickly, without taking care of a database. Now after this part you might want to:
- Secure the app with login - *Spring Security*
- Add a database - *Spring Data JPA* *DynamoDB*
- Add an nginx front
- Add  HTML5 pages with a popular js framework - *angular*, *react*
